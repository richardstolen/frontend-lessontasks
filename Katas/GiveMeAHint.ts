function grantTheHint(input: string): string {
  let result: string = "";
  const words: string[] = input.split(" ");

  // Getting the longest word with sorting
  const longestWord: number = input
    .split(" ")
    .sort((a, b) => b.length - a.length)[0].length;

  for (let i = -1; i < longestWord; i++) {
    for (const word of words) {
      result += word
        .split("")
        .map((x, j) => (j <= i ? x : "_"))
        .join("");

      result += " ";
    }
    result += "\n";
  }
  console.log(result);
  return result;
}

console.log(grantTheHint("Mary Queen of Scots"));
console.log(grantTheHint("The Life of Pi"));
